# Basic pod info

![](assets/images/dne-dci-dcloud.png)

| Hostname | Description | IP Address | Credentials |
| --- | --- | --- | --- |
| **wkst1** | Dev Workstation: Windows | 198.18.133.36 | dcloud\demouser/C1sco12345 |
| **ubuntu** | Dev Workstation: Linux | 198.18.134.28 | cisco/C1sco12345 |
| **nxosv-1** | Nexus 9000v - 7.0.3(I2).1 | 198.18.134.140 | admin/C1sco12345 |
| **nxosv-2** | Nexus 9000v - 7.0.3(I2).1 | 198.18.134.141 | admin/C1sco12345 |
| **apic1** | ACI Simulator - 2.1.1h | 198.18.133.200 | admin/C1sco12345 |
| **centos1** | CentOS 7 Server - Automation Target | 198.18.134.49 | root/C1sco12345 or demouser/C1sco12345 |
| **centos2** | CentOS 7 Server - Automation Target | 198.18.134.50 | root/C1sco12345 or demouser/C1sco12345 |
| ucsctl1 | UCS Central - 1.5(1b) | 198.18.133.90 | admin/C1sco12345 |
| ucsm1 | UCS Manager Emulator | 198.18.133.91 | admin/C1sco12345 |
| ucsd | UCS Director - 6.0.1.1 | 198.18.133.112 | admin/C1sco12345 |
| win2012r2 | Windows 2012 Standard Server - Automation Target | 198.18.133.20 | dcloud\administrator/C1sco12345 |
| vc1 | vCenter 6.0 | 198.18.133.30 | administrator@vsphere.local/C1sco12345! |
| vesx1 | vSphere Host | 198.18.133.31 | root/C1sco12345 |
| vesx2 | vSphere Host | 198.18.133.32 | root/C1sco12345 |
| cimc1 | UCS IMC Emulator | 198.18.134.88 | root/C1sco12345 or admin/C1sco12345 |
| na-edge1 | vNetApp | 198.18.133.115 | root/C1sco12345 |
| ad1 | Domain Controller | 198.18.133.1 | administrator/C1sco12345 |
| ios-xe-mgmt.cisco.com | CSR1000v - 16.08.01 | ios-xe-mgmt.cisco.com:8181 | root/D_Vay!_10& |


## Getting connected

1. Run Anyconnect client and fill in url with ```dcloud-rtp-anyconnect.cisco.com``` 

1. Click connect, and log in with the credentials from your assigned pod, listed below:

| Session Id |  Session Name |  Usernames |  Password |  Public IPs   | Pod URL |
| --- | --- | --- | --- | --- | --- |
| 536737 | 1 - Cisco DevNet Express Data Center v2 | v949user1; v949user2; ... v949user16 | e890a7 | [https://64.100.10.36:8443/](https://64.100.10.36:8443/) |
| 536736 | 2 - Cisco DevNet Express Data Center v2 | v692user1; v692user2; ... v692user16 | 4e53f0 | [https://64.100.10.36:8443/](https://64.100.10.36:8443/) |
| 536735 | 3 - Cisco DevNet Express Data Center v2 | v1219user1; v1219user2; ... v1219user16 | 9afaa7 | [https://64.100.10.36:8443/](https://64.100.10.36:8443/) |
| 536734 | 4 - Cisco DevNet Express Data Center v2 | v1172user1; v1172user2; ... v1172user16 | 47461e | [https://64.100.10.36:8443/](https://64.100.10.36:8443/) |
| 536733 | 5 - Cisco DevNet Express Data Center v2 | v897user1; v897user2; ... v897user16 | 7f7e32 | [https://64.100.10.36:8443/](https://64.100.10.36:8443/) |
| 536732 | 6 - Cisco DevNet Express Data Center v2 | v223user1; v223user2; ... v223user16 | b66073 | [https://64.100.10.36:8443/](https://64.100.10.36:8443/) |
| 536731 | 7 - Cisco DevNet Express Data Center v2 | v519user1; v519user2; ... v519user16 | ac49fb | [https://64.100.10.36:8443/](https://64.100.10.36:8443/) |
| 536730 | 8 - Cisco DevNet Express Data Center v2 | v835user1; v835user2; ... v835user16 | 9973c7 | [https://64.100.10.36:8443/](https://64.100.10.36:8443/) |
| 536729 | 9 - Cisco DevNet Express Data Center v2 | v940user1; v940user2; ... v940user16 | 364340 | [https://64.100.10.36:8443/](https://64.100.10.36:8443/) |
| 536728 | 10 - Cisco DevNet Express Data Center v2 | v682user1; v682user2; ... v682user16 | ac5f00 | [https://64.100.10.36:8443/](https://64.100.10.36:8443/) |
| 536727 | 11 - Cisco DevNet Express Data Center v2 | v931user1; v931user2; ... v931user16 | d6a14c | [https://64.100.10.36:8443/](https://64.100.10.36:8443/) |
| 536726 | 12 - Cisco DevNet Express Data Center v2 | v1160user1; v1160user2; ... v1160user16 | 576c74 | [https://64.100.10.36:8443/](https://64.100.10.36:8443/) |
| 536725 | 13 - Cisco DevNet Express Data Center v2 | v1340user1; v1340user2; ... v1340user16 | 96a718 | [https://64.100.10.36:8443/](https://64.100.10.36:8443/) |
| 536724 | 14 - Cisco DevNet Express Data Center v2 | v244user1; v244user2; ... v244user16 | 353d65 | [https://64.100.10.36:8443/](https://64.100.10.36:8443/) |
| 536723 | 15 - Cisco DevNet Express Data Center v2 | v206user1; v206user2; ... v206user16 | 23a450 | [https://64.100.10.36:8443/](https://64.100.10.36:8443/) |
| 536722 | 16 - Cisco DevNet Express Data Center v2 | v480user1; v480user2; ... v480user16 | 012fab | [https://64.100.10.36:8443/](https://64.100.10.36:8443/) |
| 536721 | 17 - Cisco DevNet Express Data Center v2 | v1056user1; v1056user2; ... v1056user16 | 349b81 | [https://64.100.10.36:8443/](https://64.100.10.36:8443/) |
| 536720 | 18 - Cisco DevNet Express Data Center v2 | v1087user1; v1087user2; ... v1087user16 | abd4d6 | [https://64.100.10.36:8443/](https://64.100.10.36:8443/) |
| 536719 | 19 - Cisco DevNet Express Data Center v2 | v1380user1; v1380user2; ... v1380user16 | a3a4b9 | [https://64.100.10.36:8443/](https://64.100.10.36:8443/) |
| 536718 | 20 - Cisco DevNet Express Data Center v2 | v154user1; v154user2; ... v154user16 | 57bd36 | [https://64.100.10.36:8443/](https://64.100.10.36:8443/) |
| 536717 | 21 - Cisco DevNet Express Data Center v2 | v1437user1; v1437user2; ... v1437user16 | 03e6dd | [https://64.100.10.36:8443/](https://64.100.10.36:8443/) |
| 536716 | 22 - Cisco DevNet Express Data Center v2 | v1086user1; v1086user2; ... v1086user16 | 7e9b87 | [https://64.100.10.36:8443/](https://64.100.10.36:8443/) |
| 536715 | 23 - Cisco DevNet Express Data Center v2 | v1182user1; v1182user2; ... v1182user16 | 07177a | [https://64.100.10.36:8443/](https://64.100.10.36:8443/) |
| 536714 | 24 - Cisco DevNet Express Data Center v2 | v1036user1; v1036user2; ... v1036user16 | 630f45 | [https://64.100.10.36:8443/](https://64.100.10.36:8443/) |
| 536713 | 25 - Cisco DevNet Express Data Center v2 | v273user1; v273user2; ... v273user16 | 55a210 | [https://64.100.10.36:8443/](https://64.100.10.36:8443/) |
| 536712 | 26 - Cisco DevNet Express Data Center v2 | v920user1; v920user2; ... v920user16 | 9584a8 | [https://64.100.10.36:8443/](https://64.100.10.36:8443/) |
| 536711 | 27 - Cisco DevNet Express Data Center v2 | v451user1; v451user2; ... v451user16 | 776566 | [https://64.100.10.36:8443/](https://64.100.10.36:8443/) |
| 536710 | 28 - Cisco DevNet Express Data Center v2 | v434user1; v434user2; ... v434user16 | 86ae96 | [https://64.100.10.36:8443/](https://64.100.10.36:8443/) |
| 536709 | 29 - Cisco DevNet Express Data Center v2 | v1487user1; v1487user2; ... v1487user16 | fa0515 | [https://64.100.10.36:8443/](https://64.100.10.36:8443/) |
| 536708 | 30 - Cisco DevNet Express Data Center v2 | v1455user1; v1455user2; ... v1455user16 | 1559b8 | [https://64.100.10.36:8443/](https://64.100.10.36:8443/) |





